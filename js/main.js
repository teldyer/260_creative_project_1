currentState = {
  "currentPlayer": "X",
  "state": "wait",
}


nanWinningCells = ['cell1', 'cell5', 'cell9'];
winningCodes = [448, 56 , 7, 292, 146, 73, 137, 84];

cellNames = ['cell1', 'cell2', 'cell3', 'cell4', 'cell5', 'cell6', 'cell7', 'cell8', 'cell9'];

function isWin(current){
  win = false;
  winningCodes.forEach(code => {
    res = current & code;
    if (res === code)
      win = true;
  });
  return win;
}

function claimCell(cell) {
  if(currentState.state != "wait"){
    if(cell.innerHTML == ""){
    cell.innerHTML = currentState.currentPlayer;
    if(checkForWin(currentState.currentPlayer)){
    document.getElementById("toaster").innerHTML = currentState.currentPlayer + " has won!";
    currentState.state = "wait";
  }
  else{
    nextTurn();
  }
  }
  
  }
  
  
}

function startGame() {
  
  if(currentState.state != "play"){
    resetGame();
    currentState.currentPlayer = "X";
    currentState.toaster =   "It's " + currentState.currentPlayer + "'s turn";
    document.getElementById("toaster").innerHTML = currentState.toaster;
    currentState.state = "play";
  }
  
}

function nextTurn() {
  currentState.currentPlayer = currentState.currentPlayer == "X" ? "O" : "X";
  
  document.getElementById("toaster").innerHTML = "It's " + currentState.currentPlayer + "'s turn";
}

function checkForWin(player) {
  currentGameCode = "";
  cellNames.forEach(cellElement => {
    if (document.getElementById(cellElement).innerHTML == player){
      currentGameCode = currentGameCode + "1";
    } else {
      currentGameCode = currentGameCode + "0";
    }
  });
  nanWinner = true;
  nanWinningCells.forEach(cellElement => {
      if (document.getElementById(cellElement).innerHTML != player)
          nanWinner = false;

  });
  if (nanWinner)
      return true;

  code = parseInt(currentGameCode, 2);
  return isWin(code);
  
}



function resetGame(){
  cellNames.forEach(vari =>{
    document.getElementById(vari).innerHTML = "";
  })
}
function reset(){
  resetGame();
  currentState.state = "wait";
  
  document.getElementById("toaster").innerHTML = "Press start to play";
  
}

// alert(checkForWin("X"))